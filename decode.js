var fs = require('fs');
var PacketDecoder = require('./lib/protocol/PacketStream');

var file = fs.createReadStream(__dirname + '/incoming.bin');

var ClientPacketParser = require('./lib/protocol/ClientPacketParser');

var decoder = new PacketStream({parser: new ClientPacketParser()});

file.pipe(decoder);