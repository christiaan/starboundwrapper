var Server = require('./lib/Server');
var Client = require('./lib/Client');
var server = new Server({
    port: 21027,
    clientFactory: function(connection) {
        return new Client(
            {forwardPort: 21025, forwardHost: '127.0.0.1'},
            connection
        );
    }
});

server.listen();