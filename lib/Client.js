var EventEmitter = require('events').EventEmitter;
var util = require('util');
var net = require('net');
var fs = require('fs');
var PacketStream = require('./protocol/PacketStream');

var packetDir = __dirname + '/../packets';
var incomingCount = 1;
var forwardCount = 1;

module.exports = Client;

function Client(options, incomingConnection) {
    EventEmitter.call(this);
    this.clientServerDecoder = new PacketStream({packetHandler: this.clientServerHandler.bind(this)});
    this.serverClientDecoder = new PacketStream({packetHandler: this.serverClientHandler.bind(this)});

    var forwardConnection = net.createConnection(options.forwardPort, options.forwardHost);
    incomingConnection.pipe(this.clientServerDecoder).pipe(forwardConnection);
    forwardConnection.pipe(this.serverClientDecoder).pipe(incomingConnection);
}

util.inherits(Client, EventEmitter);

/**
 * @param {Packet} packet
 */
Client.prototype.clientServerHandler = function (packet) {
    fs.writeFile(packetDir + '/incoming/' + packet.id + '-' + incomingCount + '.bin', packet.raw, function (err) {
        packet.accept();
    });
    incomingCount += 1;
};

/**
 * @param {Packet} packet
 */
Client.prototype.serverClientHandler = function (packet) {
    fs.writeFile(packetDir + '/forwarding/' + packet.id + '-' + incomingCount + '.bin', packet.raw, function (err) {
        packet.accept();
    });
    forwardCount += 1;
};
