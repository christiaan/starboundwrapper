var EventEmitter = require('events').EventEmitter;
var util = require('util');
var net = require('net');

module.exports = Server;

/**
 * @param options
 * @constructor
 */
function Server(options) {
    EventEmitter.call(this);

    this.clientFactory = options.clientFactory;
    this.port = options.port;

    this.clients = [];

    this.server = net.createServer();
    this.server.on('connection', this._onConnection.bind(this));
}

util.inherits(Server, EventEmitter);

Server.prototype.listen = function () {
    this.server.listen(this.port);
};

Server.prototype._onConnection = function (connection) {
    var client = this.clientFactory(connection);
    this.clients.push(client);
    client.on('disconnect', this._onDisconnect.bind(this, client));
};

Server.prototype._onDisconnect = function (client) {
    var index = this.clients.indexOf(client);
    if (index !== -1) {
        this.clients.splice(index, 1);
    }
};
