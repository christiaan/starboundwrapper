var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = StarboundStdout;

/**
 * @param {EventEmitter} stdout
 * @constructor
 */
function StarboundStdout(stdout) {
    EventEmitter.call(this);
    this.stdout = stdout;
    this.regexes = [];
    this.buffer = '';
    this.delimiter = "\r\n";

    this.onMatch(/Client\s+\'([^']*)\' .* connected/, function(matches) {
        this.emit('join', matches[1]);
    });
    this.onMatch(/Client\s+\'([^']*)\' .* disconnected/, function(matches) {
        this.emit('leave', matches[1]);
    });
    this.onMatch(/UniverseServer::run/, function() {
        this.ready = true;
        this.emit('ready');
    });

    this.stdout.on('data', this._onData.bind(this));
}

util.inherits(StarboundStdout, EventEmitter);

/**
 * @param {RegExp} regex
 * @param {function(Array)} cb
 * @returns {StarboundStdout}
 */
StarboundStdout.prototype.onMatch = function (regex, cb) {
    this.regexes.push({regex: regex, cb: cb});
    return this;
};

StarboundStdout.prototype._onData = function (data) {
    var index;
    this.buffer += data;
    while ((index = this.buffer.indexOf(this.delimiter)) !== -1) {
        this._parseLine(this.buffer.substring(0, index));
        this.buffer = this.buffer.substring(index + this.delimiter.length);
    }
};

StarboundStdout.prototype._parseLine = function(line) {
    this.regexes.forEach(function(item) {
        var matches = item.regex.exec(line);
        if (matches !== null) {
            item.cb.call(this, matches);
        }
    }, this);
};
