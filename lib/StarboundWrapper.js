var spawn = require('child_process').spawn;
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var StarboundStdout = require('./StarboundStdout');

module.exports = StarboundWrapper;

/**
 * @param options
 * @constructor
 */
function StarboundWrapper(options) {
    EventEmitter.call(this);

    this.stdout = new StarboundStdout(options.stdout);
    this.stdout.on('ready', this._onReady.bind(this));
    this.stdout.on('leave', this._onLeave.bind(this));
}

util.inherits(StarboundWrapper, EventEmitter);

StarboundWrapper.prototype._onReady = function () {
};
