module.exports = BinaryReader;

function BinaryReader(buf) {
    this.buf = buf;
    this.position = 0;
}

BinaryReader.prototype.canRead = function (amount) {
    amount = amount || 1;
    return (this.position - 1) + amount < this.buf.length;

};

/**
 * @returns {Buffer}
 */
BinaryReader.prototype.readStarByteArray = function () {
    var size = this.readVarUInt64();
    return this.readFully(size);
};

/**
 * @returns {string}
 */
BinaryReader.prototype.readStarString = function () {
    var size = this.readVarUInt64();
    if (size === 0) {
        return "";
    }
    var starString = this.buf.toString('utf8', this.position, size + 1);
    this.position += size;
    return starString;
};

/**
 * @returns {Buffer}
 */
BinaryReader.prototype.readStarUUID = function () {
    var exists = this.readBoolean();
    if(exists) {
        return this.readFully(16);
    }
    return new Buffer(16);
};

BinaryReader.prototype.__readStarWorldCoordinate = function() {
    var sector = this.readStarString();
    var x = this.readInt32BE();
    var y = this.readInt32BE();
    var z = this.readInt32BE();
    var planet = this.readInt32BE();
    var satellite = this.readInt32BE();

// TODO
//    if (StarryboundServer.config.sectors.Contains(sector))
//    {
//        if (planet < 0 || planet > 256)
//            throw new IndexOutOfRangeException("WorldCoordinate Planet out of range: " + planet);
//        if (satellite < 0 || satellite > 256)
//            throw new IndexOutOfRangeException("WorldCoordinate Satellite out of range: " + satellite);
//        return new WorldCoordinate(sector, x, y, z, planet, satellite);
//    }
//    else if (String.IsNullOrEmpty(sector))
//        return new WorldCoordinate();
//    else
//        throw new IndexOutOfRangeException("WorldCoordinate Sector out of range: " + sector);
};

BinaryReader.prototype.readStarVariant = function() {
    var i, type = this.readByte();
    switch (type) {
        case 2:
            return this.readDoubleBE();
        case 3:
            return this.readBoolean();
        case 4:
            return this.readVarInt64();
        case 5:
            return this.readStarString();
        case 6:
            var size = this.readVarUInt32();
            var variant = [];
            for(i = 0; i < size; i++) {
                variant.push(this.readStarVariant());
            }
            return variant;
        case 7:
            var size2 = this.readVarUInt32();
            var variantMap = {};
            for(i = 0; i < size2; i++)
            {
                variantMap[this.readStarString()] = this.readStarVariant();
            }
            return variantMap;
    }
    return null;
};

BinaryReader.prototype.readVarUInt32 = function() {
    return this.toTarget(32);
};

BinaryReader.prototype.readVarInt32 = function() {
    var result = this.toTarget(32);
    return decode(result);
};

BinaryReader.prototype.readVarUInt64 = function () {
    return this.toTarget(64);
};

BinaryReader.prototype.readVarInt64 = function () {
    var result = this.toTarget(64);
    return decode(result);
};

BinaryReader.prototype.readInt32BE = function () {
    if (!this.canRead(4)) {
        throw new Error('Out of bounds');
    }
    var nr = this.buf.readInt32BE(this.position);
    this.position += 4;
    return nr;
};

/**
 * @returns {boolean}
 */
BinaryReader.prototype.readBoolean = function () {
    return this.readByte() !== 0;
};

/**
 * @returns {Number}
 */
BinaryReader.prototype.readByte = function () {
    if (this.position === this.buf.length) {
        throw new Error('Out of bounds');
    }
    return this.buf[this.position++];
};

BinaryReader.prototype.toTarget = function (sizeBites) {
    var buffer = new Buffer(10);
    var pos = 0;

    var shift = 0;
    var result = 0;

    for (;;)
    {
        var byteValue = this.readByte();
        buffer[pos++] = byteValue;

        result = (result << 7) | byteValue & 0x7f;

        if (shift > sizeBites)
            throw new Error("Variable length quantity is too long. (must be " + sizeBites + ")");

        if ((byteValue & 0x80) == 0x00)
        {
            var bytes = new Buffer(pos);
            buffer.copy(bytes, 0, 0, pos);
            return result;
        }

        shift += 7;
    }

    throw new Error("Cannot decode variable length quantity from stream.");
};

/**
 * @param {Number} requiredSize
 * @returns {Buffer}
 */
BinaryReader.prototype.readFully = function (requiredSize) {
    if (!this.canRead(requiredSize)) {
        return null;
    }

    var ret = this.buf.slice(this.position, this.position + requiredSize);
    this.position = this.position + requiredSize;
    return ret;
};

function decode(value)
{
    if ((value & 1) == 0x00)
        return (value >> 1);
    else
        return -(value >> 1);
}