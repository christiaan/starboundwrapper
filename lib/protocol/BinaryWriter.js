module.exports = BinaryWriter;

/**
 * @param {Buffer} buf
 * @constructor
 */
function BinaryWriter(buf) {
    this.buf = buf;
    this.position = 0;
}

BinaryWriter.prototype.assertCanWrite = function (amount) {
    amount = amount || 1;
    if ((this.position - 1) + amount >= this.buf.length) {
        throw new Error('Out of bounds');
    }
};

BinaryWriter.prototype.write = function (buf) {
    this.assertCanWrite(buf.length);
    buf.copy(this.buf, this.position, 0, buf.length);
    this.position += buf.length;
};

BinaryWriter.prototype.writeByte = function (byte) {
    this.assertCanWrite(1);
    this.buf[this.position] = byte;
    this.position += 1;
};

BinaryWriter.prototype.writeBoolean = function (bool) {
    this.writeByte(bool ? 1 : 0);
};

BinaryWriter.prototype.writeInt32BE = function (value) {
    this.assertCanWrite(4);
    this.buf.writeInt32BE(value);
    this.position += 4;
};

BinaryWriter.prototype.writeVarUInt64 = function(value)
{
    var buffer = new Buffer(10);
    var pos = 0;
    do {
        var byteVal = value & 0x7f;
        value >>= 7;

        if (value != 0) {
            byteVal |= 0x80;
        }

        buffer[pos++] = byteVal;

    } while (value != 0);

    var result = this._bufferSliceReverse(buffer, 0, pos);

    if(pos > 1)
    {
        result[0] |= 0x80;
        result[pos-1] ^= 0x80;
    }

    this.write(result);
};

// writeVarUInt32 is just a alias to writeVarUInt64
BinaryWriter.prototype.writeVarUInt32 = BinaryWriter.prototype.writeVarUInt64;

BinaryWriter.prototype.writeStarString = function(str) {
    var len = Buffer.byteLength(str);
    this.assertCanWrite(len);
    this.writeVarUInt64(len);
    this.buf.write(str, this.position, len);
    this.position += len;
};

BinaryWriter.prototype._bufferSliceReverse = function (buf, start, end) {
    var size = end - start;
    var result = new Buffer(size);
    for (var i = end, c = 0; i >= start; i -= 1, c +=1) {
        result[c] = buf[i];
    }
    return result;
};