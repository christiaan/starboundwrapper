module.exports = PacketFactory;

function PacketFactory() {
    this.packetClasses = [
        require('./packet/Packet2ConnectResponse'),
        require('./packet/Packet5ChatReceive'),
        require('./packet/Packet7ClientConnect'),
        require('./packet/Packet11ChatSend')
    ];
}

PacketFactory.prototype.create = function (packetId, reader) {
    var packetClass;
    for (var i = 0, len = this.packetClasses.length; i < len; i += 1) {
        packetClass = this.packetClasses[i];
        if (packetClass.packetId == packetId) {
            return packetClass.read(reader);
        }
    }

    return null;
};