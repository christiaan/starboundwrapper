var util = require('util');
var Transform = require('stream').Transform;
var BinaryReader = require('./BinaryReader');

module.exports = PacketStream;

/**
 * @param {{packetIds: Array, packetHandler: function()}} options
 * @constructor
 * @extends Transform
 */
function PacketStream(options) {
    options = options || {};
    Transform.call(this);
    this.decoderBuffer = null;

    this.packetIds = options.packetIds;
    this.packetHandler = options.packetHandler;
}

util.inherits(PacketStream, Transform);

PacketStream.prototype._transform = function (chunk, encoding, cb) {

    if (this.decoderBuffer) {
        this.decoderBuffer = Buffer.concat([this.decoderBuffer, chunk]);
    } else {
        this.decoderBuffer = chunk;
    }

    var next = function(err) {
        if (err) {
            return cb(err);
        }
        if (!this.decoderBuffer.length) {
            return cb();
        }
        var read = new BinaryReader(this.decoderBuffer);
        var packetId, packetSize, compressed;
        packetId = read.readVarUInt32();
        if (null === packetId) {
            return cb();
        }

        packetSize = read.readVarInt32();
        if (packetSize === null) {
            return cb();
        }

        if (packetSize < 0) {
            packetSize = -packetSize;
            compressed = true;
        } else {
            compressed = false;
        }

        var packetBuffer = read.readFully(packetSize);

        if (packetBuffer === null) {
            return cb();
        }

        var rawPacket = this.shiftPacket(read.position);

        if (!this.packetIds || this.packetIds.indexOf(packetId) !== -1) {
            this.packetHandler(
                new Packet(
                    this, next,
                    {id: packetId, data: packetBuffer, compressed: compressed, raw: rawPacket}
                )
            );
        } else {
            this.push(rawPacket);
            process.nextTick(next);
        }
    }.bind(this);

    process.nextTick(next);
};

PacketStream.prototype.shiftPacket = function (size) {
    var rawPacket = this.decoderBuffer.slice(0, size);
    this.decoderBuffer = this.decoderBuffer.slice(size);

    return rawPacket;
};


/**
 * @param {PacketStream} decoder
 * @param {function(?String)} next
 * @param {{id: Number, data: Buffer, compressed: Boolean, raw: Buffer}} data
 * @constructor
 */
function Packet(decoder, next, data) {
    this.decoder = decoder;
    this.next = next;

    this.id = data.id;
    this.data = data.data;
    this.compressed = data.compressed;
    this.raw = data.raw;
}

Packet.prototype.accept = function () {
    this.decoder.push(this.raw);
    process.nextTick(this.next);
};

Packet.prototype.drop = function () {
    process.nextTick(this.next);
};