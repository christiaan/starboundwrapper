module.exports = Packet11ChatSend;

function Packet11ChatSend(message, context) {
    this.message = message;
    this.context = context;
}

Packet11ChatSend.Universe = 0;
Packet11ChatSend.Planet = 1;

/**
 * @param {BinaryReader} read
 */
Packet11ChatSend.read = function (read) {
    var message = read.readStarString();
    var context = read.readByte();

    return new Packet11ChatSend(message, context);
};

/**
 * @type {number}
 */
Packet11ChatSend.packetId = 11;

/**
 * @param {BinaryWriter} writer
 */
Packet11ChatSend.prototype.write = function (writer) {
    writer.writeStarString(this.message);
    writer.writeByte(this.context);
};
