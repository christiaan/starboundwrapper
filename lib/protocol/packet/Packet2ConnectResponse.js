module.exports = Packet2ConnectResponse;

function Packet2ConnectResponse(success, clientId, rejectReason) {
    this.success = success;
    this.clientId = clientId;
    this.rejectReason = rejectReason;
}

/**
 * @param {BinaryReader} reader
 */
Packet2ConnectResponse.read = function (reader) {
    var success = reader.readBoolean();
    var clientId = reader.readVarUInt32();
    var rejectReason = reader.readStarString();

    return new Packet2ConnectResponse(success, clientId, rejectReason);
};

Packet2ConnectResponse.packetId = 2;

/**
 * @param {BinaryWriter} writer
 */
Packet2ConnectResponse.prototype.write = function (writer) {
    writer.writeBoolean(this.success);
    writer.writeVarUInt32(this.clientId);
    writer.writeStarString(this.rejectReason);
};