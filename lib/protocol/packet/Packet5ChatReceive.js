module.exports = Packet5ChatReceive;

function Packet5ChatReceive(context, world, clientId, name, message) {
    this.context = context;
    this.world = world;
    this.clientId = clientId;
    this.name = name;
    this.message = message;
}

/**
 * @param {BinaryReader} reader
 */
Packet5ChatReceive.read = function (reader) {
    var context = reader.readByte();
    var world = reader.readStarString();
    var clientID = reader.readInt32BE();
    var name = reader.readStarString();
    var message = reader.readStarString();

    return new Packet5ChatReceive(context, world, clientID, name, message);
};

Packet5ChatReceive.packetId = 5;

/**
 * @param {BinaryWriter} writer
 */
Packet5ChatReceive.prototype.write = function (writer) {
    writer.writeByte(this.context);
    writer.writeStarString(this.world);
    writer.writeInt32BE(this.clientId);
    writer.writeStarString(this.name);
    writer.writeStarString(this.message);
};