module.exports = Packet7ClientConnect;

function Packet7ClientConnect(uuid, name, account, assetDigest) {
    this.uuid = uuid;
    this.name = name;
    this.account = account;
    this.assetDigest = assetDigest;
}

/**
 * @param {BinaryReader} reader
 */
Packet7ClientConnect.read = function (reader) {
    var assetDigest = reader.readStarByteArray();

    var claim = reader.readStarVariant();
    var UUID = reader.readStarUUID();
    var name = reader.readStarString();
    var species = reader.readStarString();
    var shipWorld = reader.readStarString();
    var account = reader.readStarString();

    return new Packet7ClientConnect(
        UUID.toString('base64'),
        name,
        account,
        assetDigest.toString('base64')
    );
};

/**
 * @type {number}
 */
Packet7ClientConnect.packetId = 7;

/**
 * @param {BinaryWriter} writer
 */
Packet7ClientConnect.prototype.write = function (writer) {
    throw new Error('Not implemented');
};