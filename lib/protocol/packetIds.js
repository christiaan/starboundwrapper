module.exports = {
    ProtocolVersion : 1, //Done
    ConnectResponse : 2, //Done
    ServerDisconnect : 3, //Done
    HandshakeChallenge : 4, //Done
    ChatReceive : 5, //Done
    UniverseTimeUpdate : 6, //Not Needed
    ClientConnect : 7, //Done
    ClientDisconnect : 8, //Done
    HandshakeResponse : 9, //Done
    WarpCommand : 10, //Done
    ChatSend : 11, //Done
    ClientContextUpdate : 12, //In Progress - NetStateDelta
    WorldStart : 13, //Done
    WorldStop : 14, //Done
    TileArrayUpdate : 15,
    /*
     * VLQI X
     * VLQI T
     * VLQU SizeX
     * VLQU SizeY
     * {
     * Star::NetTile
     * }
     */
    TileUpdate : 16,
    /*
     * int X
     * int Y
     * Star::NetTile
     */
    TileLiquidUpdate : 17,
    /*
     * VLQI X
     * VLQI Y
     * uchar
     * uchar
     */
    TileDamageUpdate : 18,
    /*
     * int X
     * int Y
     * uchar
     * Star::TileDamageStatus
     * {
     * float
     * float
     * float
     * float
     * float
     * }
     */
    TileModificationFailure : 19,
    /*
     * VLQU Size
     * {
     * ???
     * }
     */
    GiveItem : 20, //Done
    SwapInContainerResult : 22,
    EnvironmentUpdate : 23,
    /*
     * ByteArray
     * ByteArray
     */
    EntityInteractResult : 24,
    /*
     * uint ClientId
     * int EntityId
     * Star::Variant
     */
    ModifyTileList : 25,
    /*
     * VLQU Size
     * {
     * ???
     * }
     * bool
     */
    DamageTile : 26,
    /*
     * int X
     * int Y
     * uchar
     * [float, float]
     * uchar
     * float
     */
    DamageTileGroup : 27,
    /*
     * VLQU
     * {
     * int X
     * int Y
     * ???
     * }
     * uchar
     * [float, float]
     * uchar
     * float
     */
    RequestDrop : 28,
    /*
     * VLQI SlotId?
     */
    SpawnEntity : 29,
    /*
     * uchar Type
     * ByteArray loadArray
     */
    EntityInteract : 30,
    /*
     * int EntityId
     * float, float
     * float, float
     */
    ConnectWire : 31,
    /*
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     */
    DisconnectAllWires : 32,
    /*
     * VLQI
     * VLQI
     * VLQI
     * VLQI
     */
    OpenContainer : 33,
    CloseContainer : 34,
    SwapInContainer : 35,
    ItemApplyInContainer : 36,
    StartCraftingInContainer : 37,
    StopCraftingInContainer : 38,
    BurnContainer : 39,
    ClearContainer : 40,
    WorldClientStateUpdate : 41,  //In Progress - NetStateDelta
    EntityCreate : 42,
    /*
     * uchar Type
     * ByteArray loadArray
     * VLQI EntityId
     */
    EntityUpdate : 43,
    /*
     * VLQI EntityId
     * ByteArray loadDeltaArray
     */
    EntityDestroy : 44,
    /*
     * VLQI EntityId
     * bool
     */
    DamageNotification : 45,
    /*
     * Star::DamageNotification
     * [
     * ]
     */
    StatusEffectRequest : 46,
    /*
     * Star::StatusEffectRequest
     */
    UpdateWorldProperties : 47,
    /*
     * VLQU
     * {
     * string
     * Star::Variant
     * }
     */
    Heartbeat : 48 //Not Needed
};