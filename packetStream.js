var fs = require('fs');
var packetStream = require('./lib/protocol/PacketStream');

var stream = packetStream({packetIds: [11]});

var file = fs.createReadStream(__dirname + '/incoming.bin');

file.pipe(stream);
